#!/usr/bin/make -rf

include /usr/share/dpkg/architecture.mk

sover=1
cmake_pkg_dir=debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/cmake/CapnProto

define substitute
sed \
	-e 's/@sover@/$(sover)/g' \
	debian/$1 \
	>debian/$2
endef

ifneq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
TESTING_FLAGS := -DBUILD_TESTING=OFF
endif

CONFIGURE_FLAGS := \
	-DCMAKE_POLICY_DEFAULT_CMP0069=NEW \
	-DSOVERSION=$(sover) \
	-DWITH_OPENSSL=OFF \
	-DWITH_ZLIB=ON

%:
	dh $@ --buildsystem=cmake+ninja --builddirectory=shared
	dh $@ --buildsystem=cmake+ninja --builddirectory=static

control:
	$(call substitute,control.in,control)

override_dh_auto_configure:
	dh_auto_configure --builddirectory=shared -- \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_SKIP_RPATH=OFF \
		-DBUILD_SHARED_LIBS=ON \
		$(TESTING_FLAGS) \
		$(CONFIGURE_FLAGS)

	dh_auto_configure --builddirectory=static -- \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SHARED_LIBS=OFF \
		-DBUILD_TESTING=OFF \
		$(CONFIGURE_FLAGS)

override_dh_auto_build:
	dh_auto_build --builddirectory=shared
	dh_auto_build --builddirectory=static

override_dh_auto_install:
	dh_auto_install --builddirectory=static
	mv $(cmake_pkg_dir)/CapnProtoTargets-release.cmake releasestatic.cmake
	dh_auto_install --builddirectory=shared
	sed -i 's/RELEASE/RELEASESTATIC/; s/Release/ReleaseStatic/' releasestatic.cmake
	mv releasestatic.cmake $(cmake_pkg_dir)/CapnProtoTargets-releasestatic.cmake

execute_before_dh_install:
	$(call substitute,libcapnp.install,libcapnp$(sover).install)
	cp debian/libcapnp.symbols debian/libcapnp$(sover).symbols
